function zaszyfrowana=szyfr_cezara(tekst,p,fun)

alfabet=['aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ'];
znaki=[' '];

[w k]=size(tekst);

    for i=1:k 
    if tekst(i)==' '
       zaszyfrowana(i)=znaki(1);
    else j=feval(fun,find(alfabet==tekst(i)),2*p);
        if(j>52)
        j=j-52;
        elseif(j<0)
                j=j+52;
        end
    zaszyfrowana(i)=alfabet(j);
    end 
end